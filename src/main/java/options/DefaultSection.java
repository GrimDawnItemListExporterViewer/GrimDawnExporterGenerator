package options;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class DefaultSection implements Section {

    private final String iniName;
    private final String uiName;
    private final String iconName;
    List<Option> options = new ArrayList<Option>();

    public DefaultSection(final String iniName, final String uiName, final String iconName) {
        this.iniName = iniName;
        this.uiName = uiName;
        this.iconName = iconName;
    }

    public DefaultSection(final String iniName, final String uiName) {
        this.iniName = iniName;
        this.uiName = uiName;
        this.iconName = null;
    }

    public DefaultSection(final String iniName) {
        this.iniName = iniName;
        this.uiName = iniName;
        this.iconName = null;
    }

    @Override
    public String getIniName() {
        return iniName;
    }

    @Override
    public String getUIName() {
        return uiName;
    }

    @Override
    public void addOption(Option option) {
        options.add(option);
    }

    @Override
    public Collection<Option> getOptions() {
        return options;
    }

    @Override
    public boolean isVisibleInUI() {
        boolean[] isVisibleInUI = new boolean[]{false};
        options.forEach(opt -> {
            if(opt.isVisual())
                isVisibleInUI[0] = true;
        });
        return isVisibleInUI[0];
    }

    @Override
    public String getIconName() {
        return iconName;
    }
}
