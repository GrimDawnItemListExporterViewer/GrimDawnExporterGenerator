package options;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class DefaultOption<T> implements Option<T> {

    private final String name;
    private final String label;
    private final String tooltip;
    private final Class<T> type;
    private final T defaultValue;
    private final Visibility visibility;
    private Class UIFieldFactory;

    public enum Visibility {
        Visible(true),
        Hidden(false);

        private final boolean isVisible;

        Visibility(boolean visible) {
            this.isVisible = visible;
        }

        public boolean isVisible() {
            return isVisible;
        }
    }

    public DefaultOption(final String name,
                         final String label,
                         final String tooltip,
                         final Class<T> type,
                         final T defaultValue
                         ) {
        this.name = name;
        this.label = label;
        this.tooltip = tooltip;
        this.type = type;
        this.defaultValue = defaultValue;
        this.visibility = Visibility.Visible;
        this.UIFieldFactory = null;
    }

    public DefaultOption(final String name,
                         final String label,
                         final String tooltip,
                         final Class<T> type,
                         final T defaultValue,
                         final Visibility visibility
                         ) {
        this.name = name;
        this.label = label;
        this.tooltip = tooltip;
        this.type = type;
        this.defaultValue = defaultValue;
        this.visibility = visibility;
        this.UIFieldFactory = null;
    }

    public DefaultOption(final String name,
                         final String label,
                         final Class<T> type,
                         final T defaultValue
                         ) {
        this.name = name;
        this.label = label;
        this.tooltip = null;
        this.type = type;
        this.defaultValue = defaultValue;
        this.visibility = Visibility.Visible;
        this.UIFieldFactory = null;
    }

    public DefaultOption(final String name,
                         final Class<T> type,
                         final T defaultValue
                         ) {
        this.name = name;
        this.label = null;
        this.tooltip = null;
        this.type = type;
        this.defaultValue = defaultValue;
        this.visibility = Visibility.Hidden;
        this.UIFieldFactory = null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getTooltip() {
        return tooltip;
    }

    @Override
    public T getDefaultValue() {
        return defaultValue;
    }


    @Override
    public boolean isVisual() {
        return visibility.isVisible();
    }

    @Override
    public void setUIFieldFactory(Class UIFieldFactory) {
        this.UIFieldFactory = UIFieldFactory;
    }

    @Override
    public Class getUIFieldFactory() {
        return UIFieldFactory;
    }
}
