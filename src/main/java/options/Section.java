package options;

import java.util.Collection;

/**
 * Created by knoodrake on 09/04/2017.
 */
public interface Section {

    String getIniName();

    String getUIName();

    void addOption(Option option);

    Collection<Option> getOptions();

    boolean isVisibleInUI();

    String getIconName();
}
