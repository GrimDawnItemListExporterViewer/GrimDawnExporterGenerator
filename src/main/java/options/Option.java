package options;

import java.util.Collection;
import java.util.List;

/**
 * Created by knoodrake on 09/04/2017.
 */
public interface Option<T> {
    String getName();

    Class<T> getType();

    String getLabel();

    String getTooltip();

    T getDefaultValue();

    boolean isVisual();

    void setUIFieldFactory(Class UIFieldFactory);

    Class getUIFieldFactory();
}
