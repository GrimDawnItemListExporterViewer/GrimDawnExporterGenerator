package options;

import javafx.beans.property.*;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class Utils {

    public static String getPropertyTypeNameFromJavaType(Class clazz) {
        if(String.class.equals(clazz)) return "StringProperty";
        if(Boolean.class.equals(clazz)) return "BooleanProperty";
        return "ObjectProperty<" + clazz.getTypeName() + ">";
    }

    public static String getPropertyTypeImplNameFromJavaType(Class clazz) {
        if(String.class.equals(clazz)) return "SimpleStringProperty";
        if(Boolean.class.equals(clazz)) return "SimpleBooleanProperty";
        return "SimpleObjectProperty<>";
    }

    public static String upperCaseFirstLetter(String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    public static String getDefaultValueString(Object defaultValue, Class type) {
        if(String.class.equals(type))
            return "\"" + defaultValue + "\"";
        if(type.isEnum())
            return type.getTypeName() + "." + String.valueOf(defaultValue);
        return String.valueOf(defaultValue);
    }

    public static String getInterfaceTypeFromRealType(Class type) {
        if(String.class.equals(type)) return String.class.getSimpleName();
        if(Integer.class.equals(type)) return Integer.class.getSimpleName();
        if(Double.class.equals(type)) return Double.class.getSimpleName();
        if(Long.class.equals(type)) return Long.class.getSimpleName();
        if(Boolean.class.equals(type)) return Boolean.class.getSimpleName();
        return String.class.getSimpleName();
    }
}
