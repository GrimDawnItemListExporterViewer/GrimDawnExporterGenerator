package generator;

import options.DefaultSection;
import options.Section;
import options.Utils;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class SectionGenerator {

    private final static String NL = System.getProperty("line.separator");
    private final static String TAB = "    ";
    private final static String TAB2 = "        ";
    private final static String TAB3 = "            ";

    public String generateInterface(Section section, String optionInterfaceStr) {
        StringBuilder sb = new StringBuilder();

        sb.append(NL);
        sb.append(TAB);
        sb.append("public interface Section");
        sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
        sb.append(" extends ConfigIniSection {");
        sb.append(NL);
        sb.append(optionInterfaceStr);
        sb.append(TAB);
        sb.append("}");

        return sb.toString();
    }

    public String generateBean(Section section, String optionBeanStr) {
        StringBuilder sb = new StringBuilder();

        sb.append(NL);
        sb.append(TAB);
        sb.append("public class Section");
        sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
        sb.append("Bean extends AbstractSectionBean implements Section");
        sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
        sb.append(" {");
        sb.append(NL);
        sb.append(TAB2);
        sb.append("public Section");
        sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
        sb.append("Bean() {");
        sb.append(NL);
        sb.append(TAB3);
        sb.append("super(\"");
        sb.append(section.getUIName());
        sb.append("\", \"");
        sb.append(section.getIniName());
        if(section.getIconName() != null) {
            sb.append("\", \"");
            sb.append(section.getIconName());
            sb.append("\");");
        } else {
            sb.append("\", null);");
        }
        sb.append(NL);
        sb.append(TAB2);
        sb.append("}");
        sb.append(NL);
        sb.append(NL);
        sb.append(optionBeanStr);
        sb.append(TAB);
        sb.append("}");

        return sb.toString();
    }
}
