package generator;

import options.Option;
import options.Section;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class PropertiesGenerator {

    private final static String NL = System.getProperty("line.separator");

    public static String generateOptionLine(Option option) {
//        if(!option.isVisual()) return "";
        if(option.getLabel() == null || option.getLabel().isEmpty()) return "";

        StringBuilder sb = new StringBuilder();
        sb.append(option.getName());
        sb.append("-label=");
        sb.append(option.getLabel());
        sb.append(NL);

        if(option.getTooltip() != null && !option.getTooltip().isEmpty()) {
            sb.append(option.getName());
            sb.append("-tooltip=");
            sb.append(option.getTooltip());
            sb.append(NL);
        }
        sb.append(NL);

        return sb.toString();
    }

    public static String generateSectionLine(Section section) {
        StringBuilder sb = new StringBuilder();
        sb.append("#        ");
        sb.append(section.getIniName());
        sb.append(" (");
        sb.append(section.getUIName());
        sb.append(")");
        sb.append(NL);
        sb.append(NL);

        return sb.toString();
    }
}
