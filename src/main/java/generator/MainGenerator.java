package generator;

import options.Section;
import sun.applet.Main;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class MainGenerator {

    final public static String GENERATE_JAVA_LOCATION = "M:\\stuff\\JavaPjts\\GrimDawnItemExporter\\src\\main\\java\\knoo\\gdie\\config\\generated";
    final public static String GENERATE_PROPERTIES_LOCATION = "M:\\stuff\\JavaPjts\\GrimDawnItemExporter\\resources\\knoo\\gdie\\config\\generated";
    final public static String GENERATE_INI_LOCATION = "M:\\stuff\\JavaPjts\\GrimDawnItemExporter\\resources\\generated";

    private final static String NL = System.getProperty("line.separator");

    public static void generate(List<Section> sections) {

        final OptionGenerator optionGenerator = new OptionGenerator();
        final SectionGenerator sectionGenerator = new SectionGenerator();

        final StringBuilder beanStr = new StringBuilder();
        final StringBuilder interfaceStr = new StringBuilder();
        final StringBuilder propertiesStr = new StringBuilder();
        final StringBuilder iniFileStr = new StringBuilder();

        sections.stream().forEach(section -> {
            StringBuilder optionsBeanStr = new StringBuilder();
            StringBuilder optionsInterfaceStr = new StringBuilder();
            iniFileStr.append(IniFileGenerator.generateSectionHeader(section));
            propertiesStr.append(PropertiesGenerator.generateSectionLine(section));

            section.getOptions().stream().forEach(option -> {

                iniFileStr.append(IniFileGenerator.generateOptionLine(option));

                optionsInterfaceStr.append(optionGenerator.generateInterface(option));
                optionsInterfaceStr.append(NL);

                optionsBeanStr.append(optionGenerator.generateBean(option));
                optionsBeanStr.append(NL);

                propertiesStr.append(PropertiesGenerator.generateOptionLine(option));
            });

            interfaceStr.append(sectionGenerator.generateInterface(section, optionsInterfaceStr.toString()));
            interfaceStr.append(NL);

            beanStr.append(sectionGenerator.generateBean(section, optionsBeanStr.toString()));
            beanStr.append(NL);
        });

        String iniConfigFile = IniConfigGenerator.generate(sections, interfaceStr.toString());
        try(  PrintWriter out = new PrintWriter(GENERATE_JAVA_LOCATION + "/IniConfig.java" )  ){
            out.println( iniConfigFile );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String iniConfigBeanFile = IniConfigBeanGenerator.generate(sections, beanStr.toString());
        try(  PrintWriter out = new PrintWriter(GENERATE_JAVA_LOCATION + "/IniConfigBean.java" )  ){
            out.println( iniConfigBeanFile );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try(  PrintWriter out = new PrintWriter(GENERATE_PROPERTIES_LOCATION + "/IniConfigBean.properties" )  ){
            out.println( propertiesStr.toString() );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try(  PrintWriter out = new PrintWriter(GENERATE_INI_LOCATION + "/config.ini" )  ){
            out.println( iniFileStr.toString() );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
