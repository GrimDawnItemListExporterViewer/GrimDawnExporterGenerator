package generator;

import options.Section;
import options.Utils;

import java.util.List;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class IniConfigBeanGenerator {

    private final static String NL = System.getProperty("line.separator");
    private final static String TAB = "    ";
    private final static String TAB2 = "        ";
    private final static String TAB3 = "            ";

    public static String generate(List<Section> sections, final String iniConfigContentStr) {
        StringBuilder sb = new StringBuilder();


        sb.append("package knoo.gdie.config.generated;");
        sb.append(NL);
        sb.append(NL);
        sb.append("import java.util.List;");
        sb.append(NL);
        sb.append("import com.dooapp.fxform.FXForm;");
        sb.append(NL);
        sb.append("import com.dooapp.fxform.annotation.NonVisual;");
        sb.append(NL);
        sb.append("import knoo.gdie.misc.HiddenSetting;");
        sb.append(NL);
        sb.append("import knoo.gdie.Main;");
        sb.append(NL);
        sb.append("import com.dooapp.fxform.annotation.FormFactory;");
        sb.append(NL);
        sb.append("import javafx.beans.property.*;");
        sb.append(NL);
        sb.append("import knoo.gdie.config.*;");
        sb.append(NL);
        sb.append("import org.ini4j.Wini;");
        sb.append(NL);
        sb.append("import org.ini4j.Profile;");
        sb.append(NL);
        sb.append("import knoo.gdie.config.*;");
        sb.append(NL);
        sb.append("import java.util.ArrayList;");
        sb.append(NL);
        sb.append(NL);
        sb.append("public class IniConfigBean implements IniConfig {");
        sb.append(NL);
        sb.append(NL);

        sections.forEach(section -> {
            sb.append(TAB);
            sb.append("private Section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append(" section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append(" = new Section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append("Bean();");
            sb.append(NL);

        });
        sb.append(NL);
        sb.append(NL);

        sb.append(TAB);
        sb.append("@Override");
        sb.append(NL);
        sb.append(TAB);
        sb.append("public List<ConfigIniSection> getSections() {");
        sb.append(NL);
        sb.append(TAB2);
        sb.append("List<ConfigIniSection> sections = new ArrayList<>();");
        sb.append(NL);
        sections.forEach(section -> {
            sb.append(TAB2);
            sb.append("sections.add(section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append(");");
            sb.append(NL);
        });
        sb.append(TAB2);
        sb.append("return sections;");
        sb.append(NL);
        sb.append(TAB);
        sb.append("}");
        sb.append(NL);
        sb.append(NL);


        sb.append(TAB);
        sb.append("@Override");
        sb.append(NL);
        sb.append(TAB);
        sb.append("public void initFrom(Wini iniSource) {");
        sb.append(NL);
        sections.forEach(section -> {
            sb.append(TAB2);
            sb.append("Profile.Section ");
            sb.append(section.getIniName());
            sb.append(" = iniSource.get(\"");
            sb.append(section.getIniName());
            sb.append("\");");
            sb.append(NL);
            sb.append(TAB2);
            sb.append("if(");
            sb.append(section.getIniName());
            sb.append(" != null) ");
            sb.append(section.getIniName());
            sb.append(".to(Main.getConfig().getSection");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append("());");
            sb.append(NL);
            sb.append(NL);
        });
        sb.append(TAB);
        sb.append("}");
        sb.append(NL);
        sb.append(NL);


        sb.append(TAB);
        sb.append("public List<FXForm> getSectionsAsFXForms() {");
        sb.append(NL);
        sb.append(TAB2);
        sb.append("List<FXForm> fxForms = new ArrayList<>();");
        sb.append(NL);
        sb.append(TAB2);
        sb.append("if(Main.getConfig().getSectionMainSettings().getShowAllSettings()) {");
        sb.append(NL);
        sections.forEach(section -> {
            sb.append(TAB3);
            sb.append("fxForms.add(new FXForm<>(section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append("));");
            sb.append(NL);
        });
        sb.append(TAB2);
        sb.append("} else {");
        sb.append(NL);
        sections.forEach(section -> {
            if(section.isVisibleInUI()) {
                sb.append(TAB3);
                sb.append("fxForms.add(new FXForm<>(section");
                sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
                sb.append("));");
                sb.append(NL);
            }
        });
        sb.append(TAB2);
        sb.append("}");
        sb.append(NL);
        sb.append(TAB2);
        sb.append("return fxForms;");
        sb.append(NL);
        sb.append(TAB);
        sb.append("}");

        sb.append(iniConfigContentStr);
        sb.append(NL);

        sections.forEach(section -> {
            sb.append(TAB);
            sb.append("@Override ");
            sb.append("public Section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append(" getSection");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append("() {");
            sb.append(NL);
            sb.append(TAB2);
            sb.append("return section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append(";");
            sb.append(NL);
            sb.append(TAB);
            sb.append("}");
            sb.append(NL);
        });

        sb.append(NL);
        sb.append("}");

        return sb.toString();
    }
}
