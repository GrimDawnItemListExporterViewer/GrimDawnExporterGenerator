package generator;

import options.Option;
import options.Utils;

import javax.rmi.CORBA.Util;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class OptionGenerator {

    private final static String NL = System.getProperty("line.separator");
    private final static String TAB = "    ";
    private final static String TAB2 = "        ";
    private final static String TAB3 = "            ";

    public String generateInterface(Option option) {
        StringBuilder sb = new StringBuilder();

        sb.append(TAB2);
        sb.append("public ");
        sb.append(Utils.getInterfaceTypeFromRealType(option.getType()));
        sb.append(" get");
        sb.append(Utils.upperCaseFirstLetter(option.getName()));
        sb.append("();");
        sb.append(NL);

        sb.append(TAB2);
        sb.append("public void");
        sb.append(" set");
        sb.append(Utils.upperCaseFirstLetter(option.getName()));
        sb.append("(");
        sb.append(Utils.getInterfaceTypeFromRealType(option.getType()));
        sb.append(" ");
        sb.append(option.getName());
        sb.append(");");
        sb.append(NL);

        return sb.toString();
    }

    public String generateBean(Option option) {
        StringBuilder sb = new StringBuilder();

        sb.append(TAB2);
        sb.append("// ");
        sb.append(option.getName());
        sb.append(NL);

        // Property
        if(option.getUIFieldFactory() != null) {
            sb.append(TAB2);
            sb.append("@FormFactory(");
            sb.append(option.getUIFieldFactory().getName());
            sb.append(".class)");
            sb.append(NL);
        }
        sb.append(TAB2);
        if(!option.isVisual()) {
            sb.append("@HiddenSetting ");
        }
        sb.append("private ");
        sb.append(Utils.getPropertyTypeNameFromJavaType(option.getType()));
        sb.append(" ");
        sb.append(option.getName());
        sb.append(" = new ");
        sb.append(Utils.getPropertyTypeImplNameFromJavaType(option.getType()));
        sb.append("(");
        if(option.getDefaultValue() != null) {
            sb.append(Utils.getDefaultValueString(option.getDefaultValue(), option.getType()));
        }
        sb.append(");");
        sb.append(NL);

        // Getter
        sb.append(TAB2);
        sb.append("@Override ");
        sb.append("public ");
        sb.append(Utils.getInterfaceTypeFromRealType(option.getType()));
        sb.append(" get");
        sb.append(Utils.upperCaseFirstLetter(option.getName()));
        sb.append("() { ");
        sb.append("return ");
        sb.append(option.getName());
        sb.append(".get()");
        if(option.getType().isEnum()) {
            sb.append(".name()");
        }
        sb.append("; ");
        sb.append("}");
        sb.append(NL);

        // Setter
        sb.append(TAB2);
        sb.append("@Override ");
        sb.append("public void ");
        sb.append(" set");
        sb.append(Utils.upperCaseFirstLetter(option.getName()));
        sb.append("(");
        sb.append(Utils.getInterfaceTypeFromRealType(option.getType()));
        sb.append(" ");
        sb.append(option.getName());
        sb.append(") { ");
        sb.append("this.");
        sb.append(option.getName());
        sb.append(".set(");
        if(option.getType().isEnum()) {
            sb.append(option.getType().getName());
            sb.append(".valueOf(");
        }
        sb.append(option.getName());
        if(option.getType().isEnum()) {
            sb.append(".toUpperCase())");
        }
        sb.append("); ");
        sb.append("}");
        sb.append(NL);
        sb.append(NL);

        return sb.toString();
    }
}
