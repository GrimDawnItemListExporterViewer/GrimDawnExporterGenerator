package generator;

import options.Section;
import options.Utils;

import java.util.List;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class IniConfigGenerator {

    private final static String NL = System.getProperty("line.separator");
    private final static String TAB = "    ";
    private final static String TAB2 = "        ";
    private final static String TAB3 = "            ";

    public static String generate(List<Section> sections, final String iniConfigContentStr) {
        StringBuilder sb = new StringBuilder();

        sb.append("package knoo.gdie.config.generated;");
        sb.append(NL);
        sb.append(NL);
        sb.append("import java.util.List;");
        sb.append(NL);
        sb.append("import org.ini4j.Wini;");
        sb.append(NL);
        sb.append("import knoo.gdie.config.*;");
        sb.append(NL);
        sb.append(NL);
        sb.append("public interface IniConfig {");
        sb.append(NL);
        sb.append(NL);

        sections.stream().forEach(section -> {
            sb.append(TAB);
            sb.append("public Section");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append(" getSection");
            sb.append(Utils.upperCaseFirstLetter(section.getIniName()));
            sb.append("();");
            sb.append(NL);
        });
        sb.append(NL);

        sb.append(TAB);
        sb.append("public List<ConfigIniSection> getSections();");
        sb.append(NL);
        sb.append(NL);
        sb.append(TAB);
        sb.append("public void initFrom(Wini iniSource);");
        sb.append(NL);
        sb.append(NL);
        sb.append(iniConfigContentStr);
        sb.append(NL);
        sb.append(NL);
        sb.append("}");

        return sb.toString();
    }

}
