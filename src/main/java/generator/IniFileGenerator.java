package generator;

import options.Option;
import options.Section;

/**
 * Created by knoodrake on 09/04/2017.
 * TODO
 */
public class IniFileGenerator {

    private final static String NL = System.getProperty("line.separator");

    public static String generateSectionHeader(Section section) {
        StringBuilder sb = new StringBuilder();
        sb.append(NL);
//        if(!section.getUIName().equals(section.getIniName())) {
//            sb.append("; ");
//            sb.append(section.getUIName());
//            sb.append(NL);
//        }
        sb.append("[");
        sb.append(section.getIniName());
        sb.append("]");
        sb.append(NL);
        return sb.toString();
    }

    public static String generateOptionLine(Option option) {
        StringBuilder sb = new StringBuilder();

        if(option.getLabel() != null || option.getTooltip() != null) {
            sb.append(NL);
        }
        if(option.getLabel() != null) {
            sb.append("; ");
            sb.append(option.getLabel());
            sb.append(NL);
        }
        if(option.getTooltip() != null) {
            sb.append(";   ");
            sb.append(option.getTooltip());
            sb.append(NL);
        }

        sb.append(option.getName());
        sb.append("=");
        if(option.getDefaultValue() != null) {
            sb.append(option.getDefaultValue());
        }
        sb.append(NL);
        return sb.toString();
    }
}
