import com.dooapp.fxform.view.factory.impl.TextAreaFactory;
import generator.MainGenerator;
import knoo.gdie.config.OutputFormat;
import knoo.gdie.ui.directoryChooser.DirectoryFieldFactory;
import knoo.gdie.ui.textarea.EscapedTextAreaFactory;
import options.DefaultOption;
import options.DefaultSection;
import options.Section;

import java.util.ArrayList;
import java.util.List;

import static options.DefaultOption.Visibility.Hidden;
import static options.DefaultOption.Visibility.Visible;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class Main {

    public static void main(String[] args) {
        List<Section> sections = new ArrayList<>();

        DefaultSection mainSettings = new DefaultSection("mainSettings", "Main settings", "fa-gears");
        DefaultOption<String> savePath = new DefaultOption<>("savePath", "Save Path", "This field is requiered", String.class, "");
        savePath.setUIFieldFactory(DirectoryFieldFactory.class);
        mainSettings.addOption(savePath);
        mainSettings.addOption(new DefaultOption<>("outputFile", "Output to file: ", null, String.class, "", Hidden));
        mainSettings.addOption(new DefaultOption<>("useTemporaryFile", "Or just use a temporary file ?", "if neither output file is filled nor use tmp file checked, it will just output in textual form in the log bellow", Boolean.class, true, Hidden));
        mainSettings.addOption(new DefaultOption<>("autoOpenFile", "Auto open", "Auto-Open the generated file with default OS associated programm", Boolean.class, true, Hidden));
        mainSettings.addOption(new DefaultOption<>("outputFormat", "Format", "default JSHTML, for trade on forum choose BBCODE", OutputFormat.class, OutputFormat.JSHTML));
        mainSettings.addOption(new DefaultOption<>("useEmbededViewer", "Use embeded viewer", "instead of the default OS programm", Boolean.class, true, Visible));
        mainSettings.addOption(new DefaultOption<>("linkItemsItemPlaceholder", String.class, "##ITEM##"));
        mainSettings.addOption(new DefaultOption<>("linkItemsQuery", String.class, "http://www.grimtools.com/db/search?query=##ITEM##&in_description=0&exact_match=1"));
        mainSettings.addOption(new DefaultOption<>("debug", Boolean.class, false));
        mainSettings.addOption(new DefaultOption<>("showAllSettings", "Show All advanced avaible settings", "YOU NEED TO RESTART THE APP TO SEE THE CHANGE", Boolean.class, false, Visible));
        sections.add(mainSettings);

        DefaultSection forum = new DefaultSection("forum");
        forum.addOption(new DefaultOption<>("linkItems", Boolean.class, true));
        forum.addOption(new DefaultOption<>("useColors", Boolean.class, true));
        forum.addOption(new DefaultOption<>("legendaryColorName", String.class, "DarkOrchid"));
        forum.addOption(new DefaultOption<>("epicColorName", String.class, "RoyalBlue"));
        forum.addOption(new DefaultOption<>("rareColorName", String.class, "Lime"));
        forum.addOption(new DefaultOption<>("useBBCodeList", Boolean.class, false));
        forum.addOption(new DefaultOption<>("mentionNumberOwned", Boolean.class, true));
        forum.addOption(new DefaultOption<>("mentionIfIsASet", Boolean.class, true));
        sections.add(forum);

        DefaultSection html = new DefaultSection("html");
        html.addOption(new DefaultOption<>("onlyOutputTheList", Boolean.class, false));
        html.addOption(new DefaultOption<>("noStyle", Boolean.class, false));
        html.addOption(new DefaultOption<>("stylesheetUri", String.class, ""));
        html.addOption(new DefaultOption<>("stylesheetEmbeed", String.class, ""));
        html.addOption(new DefaultOption<>("linkItems", Boolean.class, true));
        sections.add(html);

        DefaultSection csv = new DefaultSection("csv");
        csv.addOption(new DefaultOption<>("separator", String.class, ","));
        csv.addOption(new DefaultOption<>("skipHeaderRow", Boolean.class, false));
        sections.add(csv);

        DefaultSection items = new DefaultSection("items", "items filters", "fa-filter");
        items.addOption(new DefaultOption<>("skipItems", "Items to skip", "exact item names, comma separated", String.class, "", Hidden));
        items.addOption(new DefaultOption<>("skipItemsSearch", "Items to skip (search)", "any item that match. wildcard allowed \"*\"", String.class, "blood*thon"));
        items.addOption(new DefaultOption<>("skipCharacter", "Characters to skip", null, String.class, "", Hidden));
        items.addOption(new DefaultOption<>("skipEquippedItems", "Skip Worn gear", "do not take currently equipped items into account", Boolean.class, true));
        items.addOption(new DefaultOption<>("skipPersonalStash", "Skip personal stashes", Boolean.class, false));
        items.addOption(new DefaultOption<>("skipSharedStash", "Skip shared stash", null, Boolean.class, false, Hidden));
        items.addOption(new DefaultOption<>("skipSharedStashTab", "skip shared stash tabs", "comma separated position of the tab", String.class, "", Hidden));
        items.addOption(new DefaultOption<>("skipCharacterInventory", "Skip Inventories", Boolean.class, false));
        items.addOption(new DefaultOption<>("skipCharacterInventoryTab", "Skip Inventory tabs (for all chars)", "comma separated position of the tab", String.class, "", Hidden));
        items.addOption(new DefaultOption<>("outputLegendaries", "Legendaries Items/recipes", Boolean.class, true));
        items.addOption(new DefaultOption<>("skipLegendaryNoSet", "Only set legendaries", "skip the legendary items that are not part of a set", Boolean.class, false));
        items.addOption(new DefaultOption<>("outputEpics", "Epic Items/recipes", Boolean.class, true));
        items.addOption(new DefaultOption<>("skipEpicNoSet", "Only set epics", "skip the epic items that are not part of a set", Boolean.class, true));
        items.addOption(new DefaultOption<>("outputRares", "Monster Infrquent Items", Boolean.class, true));
        // TODO see main exporter project. unfinished there.
//        items.addOption(new DefaultOption<>("outputExaltedThreads", "Exalted Threads", "Also includes Exalted Threads that have 2 affixes", Boolean.class, true, Visible));
        items.addOption(new DefaultOption<>("outputBlueprintsAndRecipies", "Include recipes and blueprints", Boolean.class, true));
        items.addOption(new DefaultOption<>("outputNonDuplicate", "Include non-duplicates Items", "items are considerated duplicate or not AFTER the other excluding filters", Boolean.class, false));
        sections.add(items);



        StringBuilder customFilterDefaultValue = new StringBuilder();

        customFilterDefaultValue.append("/**\n");
        customFilterDefaultValue.append("  Return value: boolean (return false to skip the item)\n");
        customFilterDefaultValue.append("  Parameter: variable \"item\"\n");
        customFilterDefaultValue.append("  Structure (same as if you choose JSON output format): \n");
        customFilterDefaultValue.append("  {  \n");
        customFilterDefaultValue.append("      \"name\":\"Awesome Big item that I own\",\n");
        customFilterDefaultValue.append("      \"baseName\":\"Big item\",\n");
        customFilterDefaultValue.append("      \"prefix\":\"Awesome\",\n");
        customFilterDefaultValue.append("      \"suffix\":\"that I own\",\n");
        customFilterDefaultValue.append("      \"owners\":[  \n");
        customFilterDefaultValue.append("          \"My Character\",\n");
        customFilterDefaultValue.append("          \"My Other Character WhoGotOneToo\"\n");
        customFilterDefaultValue.append("      ],\n");
        customFilterDefaultValue.append("      \"isPartOfASet\": true,\n");
        customFilterDefaultValue.append("      \"rarity\":\"Epic\",\n");
        customFilterDefaultValue.append("      \"baseItemType\":\"Armor\",\n");
        customFilterDefaultValue.append("      \"ItemType\":\"Head\",\n");
        customFilterDefaultValue.append("      \"qty\":2\n");
        customFilterDefaultValue.append("  }\n");
        customFilterDefaultValue.append("  Sample: \n");
        customFilterDefaultValue.append("**/\n");
        customFilterDefaultValue.append("if( item.prefix == \"Awesome\" ) {\n");
        customFilterDefaultValue.append("    return false;\n");
        customFilterDefaultValue.append("}\n");
        customFilterDefaultValue.append("return true;\n");

        DefaultSection customFilter = new DefaultSection("customFilter");
        customFilter.addOption(new DefaultOption<>("useCustomJSFilter", null, null, Boolean.class, false, Hidden));
        DefaultOption<String> customJSFilter = new DefaultOption<>("customJSFilter", null, null, String.class,
                customFilterDefaultValue.toString()
                        .replaceAll("\"","\\\\\"")
                        .replaceAll("\n","\\\\n")
                , Hidden);
        customJSFilter.setUIFieldFactory(EscapedTextAreaFactory.class);
        customFilter.addOption(customJSFilter);
        sections.add(customFilter);


        MainGenerator.generate(sections);
    }
}